# screenos2dbedit

Parser to move ScreenOS FW config to Check Point dbedit

# Run

This should be run on a system having python3, simply run

````
./screenos2dbedit
````

This would ask for the config file address. Upon successful execution,
three folders would vbe created as well as a `.out` file which
contains a full report on the result of parsing.

`_1_name_mapping` : contains name replacements done to adhere to Check
Point R77 convention.

`_2_parse_result` : contains csv files for service, service group,
network, and network group objects; access rules and nat rules; and a
WARNING log. These csv files could be used to easily generate
`mgmt_cli` scripts for Check Point R80+.

`_3_dbedit_script` : contains dbedit scripts for creating
objects. (rule creating scripts are not included in version 1.0)

# GNU GPL v3+

Copyright (C) 2018 Mohammadreza Khellat and Mohammad Masoumi GNU GPL v3+

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

See also https://www.gnu.org/licenses/gpl.html
